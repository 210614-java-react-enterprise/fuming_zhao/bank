
create table bank_user(
	email varchar(100) primary key,
	password varchar(100)
);

create table bank_account(
	card_number varchar(100) primary key,
	is_saving boolean,
	balance numeric(6,2),
	shared_user_email varchar(100),
	user_email varchar(100) references bank_user
);

create table record(
	id serial primary key,
	amount integer,
	record_time timestamp,
	card_number varchar(100) references bank_account
);

insert into bank_user values ('e5','p5');
insert into bank_user values ('e6','p6');
insert into bank_account (card_number, is_saving, balance, user_email) values ('1111111111111111',true, 1000, 'e5');
insert into bank_account (card_number, is_saving, balance, user_email) values ('2222222222222222',false,1000, 'e5');
