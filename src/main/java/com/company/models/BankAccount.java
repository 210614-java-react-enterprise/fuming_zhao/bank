package com.company.models;

import java.util.Objects;

public class BankAccount {
    private String cardNumber;
    private boolean isSaving;
    private double balance;
    private String shared_user_email;
    private String user_email;

    public BankAccount(String cardNumber, boolean isSaving, double balance, String user_email) {
        this.cardNumber = cardNumber;
        this.isSaving = isSaving;
        this.balance = balance;
        this.user_email = user_email;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public boolean isSaving() {
        return isSaving;
    }

    public void setSaving(boolean saving) {
        isSaving = saving;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getShared_user_email() {
        return shared_user_email;
    }

    public void setShared_user_email(String shared_user_email) {
        this.shared_user_email = shared_user_email;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "cardNumber=" + cardNumber +
                ", isSaving=" + isSaving +
                ", balance=" + balance +
                ", shared_user_email='" + shared_user_email + '\'' +
                ", user_email='" + user_email + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccount that = (BankAccount) o;
        return isSaving == that.isSaving && Double.compare(that.balance, balance) == 0 && Objects.equals(cardNumber, that.cardNumber) && Objects.equals(shared_user_email, that.shared_user_email) && Objects.equals(user_email, that.user_email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardNumber, isSaving, balance, shared_user_email, user_email);
    }

}
