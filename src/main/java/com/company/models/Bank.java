package com.company.models;

import com.company.daos.UserDaoImpl;
import com.company.daos.BankAccountDaoImpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLOutput;
import java.util.*;


public class Bank {
    static Scanner sc = new Scanner(System.in);
    static int choice;
    static int choice2;
    static User currentUser;
    static BankAccount currentBankAccount;

    public static void prompt(){
        System.out.println("1. Register");
        System.out.println("2. Login");
        System.out.println("3. Exit");
        System.out.println("Please enter your choice :");
    }

    public static void prompt2(){
        System.out.println("1. Create an bank account");
        System.out.println("2. access existing bank account");
        System.out.println("3. log out");
        System.out.println("Please enter your choice :");
    }
    public static void prompt3(){
        System.out.println("1. deposit funds into this account");
        System.out.println("2. withdraw funds from this account");
        System.out.println("3. check your card balance");
        System.out.println("4. view transaction history");
        System.out.println("5. transfer money to other account");
        System.out.println("6. add/change authorized user (joint account)");
        System.out.println("7. go back to previous menu");
        System.out.println("Please enter your choice :");
    }

    public static void promptCheckOrSaving(){
        System.out.println("1. Checking account");
        System.out.println("2. Saving account");
        System.out.println("Please enter your choice :");
    }

    public static String generateRandomCardNumber(){
        Random rand = new Random(); //instance of random class
        String cardNumber = "";
        for(int i=0; i<16; i++){
            cardNumber += Integer.toString(rand.nextInt(10));
        }
        return cardNumber;
    }

    public static void processAccountQuery(){
        int choice3 = 0;
        while(true){
            prompt3();
            try{
                choice3 = sc.nextInt();
            }catch(InputMismatchException e){
                System.out.println("Invalid input. Please try again");
                choice3 = 0;
            }
            sc.nextLine();
            switch(choice3){
                case 1: //deposit
                    System.out.println("Please enter deposit amount");
                    try{
                        Double depositAmount = Double.parseDouble(sc.nextLine());
                        if(depositAmount >= 0){
                            BankAccountDaoImpl.deposit(currentBankAccount, depositAmount );
                        }else{
                            System.out.println("failed: negative deposits. Please try it again ");
                        }
                    }catch(NumberFormatException e) {
                        System.out.println("Invalid input. ");
                    }
                    break;
                case 2: //withdraw
                    System.out.println("Please enter withdraw amount");
                    try{
                        Double withdrawAmount = Double.parseDouble(sc.nextLine());
                        if(withdrawAmount >= 0){
                            BankAccountDaoImpl.withdraw(currentBankAccount, withdrawAmount);
                        }else{
                            System.out.println("failed: negative withdrawals. Please try it again ");
                        }
                    }catch(NumberFormatException e){
                        System.out.println("Invalid input. ");
                    }
                    break;
                case 3: // check balance
                    System.out.println("Current balance: " + String.format("%.2f", currentBankAccount.getBalance()));
                    break;
                case 4:
                    BankAccountDaoImpl.displayTransactionHistory(currentBankAccount.getCardNumber());
                    break;
                case 5:
                    System.out.println("Please enter the target card number");
                    String targetNumber = sc.nextLine();
                    System.out.println("How much do you want to transfer");
                    try{
                        Double transferAmount = Double.parseDouble(sc.nextLine());
                        if(transferAmount >= 0){
                            BankAccountDaoImpl.transfer(currentBankAccount, transferAmount, targetNumber);
                        }else{
                            System.out.println("failed: negative transfer. Please try it again ");
                        }
                    }catch(NumberFormatException e){
                        System.out.println("Invalid input. ");
                    }


                    break;
                case 6:
                    System.out.println("Please enter his/her email");
                    String jointEmail = sc.nextLine();
                    System.out.println("Please enter his/her password");
                    String jointPassword = sc.nextLine();
                    if(BankAccountDaoImpl.setJointUser(currentBankAccount, jointEmail, jointPassword) == false){
                        System.out.println("failed: add/change authorized user (joint account)");
                    };
                    break;
                case 7:
                    currentBankAccount = null;
                    return;
                default:
                    System.out.println("invalid choice. Please try again");
            }
            System.out.println();
        }
    }


    public static void main(String[] args) {

        System.out.println("Welcome to database Bank!");
        while(true){ //use break to go back previous level
            prompt();
            try{
                choice = sc.nextInt();
            }catch(InputMismatchException e){
                System.out.println("Invalid input. Please try again");
                choice = 0;
                sc.nextLine();
                continue;
            }
            sc.nextLine();
            String email = "";
            String password = "";
            boolean registered = false;
            switch(choice){
                case 1: //register
                    System.out.println("Enter your email address");
                    email = sc.nextLine();
                    System.out.println("Enter your password");
                    password = sc.nextLine();
                    currentUser = UserDaoImpl.Register(email, password);
                    if(currentUser == null){
                        System.out.println();
                        break;
                    }
                    choice = 2;
                case 2: // sign in
                    if(currentUser==null) {
                        System.out.println("Enter your email address");
                        email = sc.nextLine();
                        System.out.println("Enter your password");
                        password = sc.nextLine();
                        currentUser = UserDaoImpl.signIn(email, password);
                        if(currentUser == null){
                            System.out.println();
                            break;
                        }
                    }
                    int choice2 = 0;
                    while(choice2!= 3) {
                        prompt2();
                        boolean created = false;
                        try{
                            choice2 = sc.nextInt();
                        }catch(InputMismatchException e){
                            System.out.println("Invalid input. Please try again");
                            choice2 = 0;
                            sc.nextLine();
                            continue;
                        }
                        sc.nextLine();
                        switch (choice2) {
                            case 1: //create bank account
                                int choiceCheckOrSaving = 0;
                                promptCheckOrSaving();
                                try{
                                    choiceCheckOrSaving = sc.nextInt();
                                }catch(InputMismatchException e){
                                    System.out.println("Invalid input. default: Checking");
                                    choice2 = 1;
                                }
                                sc.nextLine();
                                boolean isSaving = false;
                                switch (choiceCheckOrSaving) {
                                    case 1:
                                        isSaving = false;
                                        break;
                                    case 2:
                                        isSaving = true;
                                        break;
                                    default:
                                        System.out.println("Invalid input. default: Checking");
                                        isSaving = false;
                                }
                                currentBankAccount = BankAccountDaoImpl.createBankAccount(generateRandomCardNumber(), isSaving, 0, currentUser.getEmail());
                                processAccountQuery();
                                break;
                            case 2: //access existing bank account
                                ArrayList<String> listOfBank = BankAccountDaoImpl.getAllExistingAccount(currentUser.getEmail());
                                if (listOfBank == null) {
                                    continue;
                                } else {
                                    int whichAccount = 0;
                                    try{
                                        whichAccount = sc.nextInt();
                                        sc.nextLine();
                                        try{
                                            currentBankAccount = BankAccountDaoImpl.getBankAccount(listOfBank.get(whichAccount - 1));
                                            processAccountQuery();
                                        }catch (IndexOutOfBoundsException e){
                                            System.out.println("Invalid input. Please try it again");
                                            sc.nextLine();
                                        }

                                    }catch(InputMismatchException e){
                                        System.out.println("Invalid input. Please try it again");
                                        sc.nextLine();
                                    }

                                }
                                break;
                            case 3:
                                System.out.println("log out");
                                currentUser = null;
                                break;
                            default:
                                System.out.println("invalid choice. Please try again");
                        }
                        System.out.println();
                    }
                    break;

                case 3:
                    System.out.println("Program ends. Thank you for using database Bank");
                    System.exit(0);
                    //break;
                default:
                    System.out.println("invalid choice. Please try again");

            } // end of first switch
            System.out.println();
        } //end of first while
    } // end of main method
}
