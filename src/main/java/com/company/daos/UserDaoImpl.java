package com.company.daos;

import com.company.models.User;
import com.company.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDaoImpl implements UserDao{
    /***
     * register a user in database
     * @param email
     * @param password
     * @return
     */
    public static User Register(String email, String password) {
        String sql = "insert into bank_user values (?,?)";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);) {
            ps.setString(1,email);
            ps.setObject(2,password);
            int success = ps.executeUpdate();
            System.out.println("Register account successfully");
            if(success>0){
                return new User(email, password);
            }
        } catch (SQLException throwables) {
            //throwables.printStackTrace();
            System.out.println("This email already exists. Please sign in");
        }
        return null;

    }

    /***
     * validate a given email and password in database
     * @param email
     * @param password
     * @return
     */
    public static User signIn(String email, String password) {
        String sql = "select * from bank_user where email = ?";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);) {
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                if (password.equals(rs.getString("password"))) {
                    System.out.println("log in successfully");
                    return new User(email, password);
                } else {
                    System.out.println("invalid password");
                }
            } else {
                System.out.println("account does not exist. Please register first");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
}
