package com.company.daos;

import com.company.models.BankAccount;
import com.company.models.User;
import com.company.util.ConnectionUtil;
import org.postgresql.util.PSQLException;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class BankAccountDaoImpl {
    /***
     * insert a bank entry in database
     * @param cardNumber
     * @param isSaving
     * @param balance
     * @param user_email
     * @return
     */
    public static BankAccount createBankAccount(String cardNumber, boolean isSaving, double balance, String user_email) {
        String sql = "insert into bank_account (card_number, is_saving, balance, user_email) values (?,?,?,?)";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);) {
            ps.setString(1,cardNumber);
            ps.setBoolean(2, isSaving);
            ps.setDouble(3, balance);
            ps.setString(4, user_email);
            int success = ps.executeUpdate();
            if(success>0){
                System.out.println("Create bank account successfully");
                System.out.println("Card number: " + cardNumber);
                System.out.println();
                return new BankAccount(cardNumber, isSaving, balance, user_email);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;

    }

    /***
     * get the bank account data given card number
     * @param cardNumber
     * @return
     */
    public static BankAccount getBankAccount(String cardNumber) {
        String sql = "select * from bank_account where card_number = ?";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);) {
            ps.setString(1,cardNumber);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                return new BankAccount(cardNumber, rs.getBoolean("is_saving"), rs.getDouble("balance"), rs.getString("user_email"));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;

    }

    /***
     * add/set another user for this bank account
     * @param account
     * @param jointEmail
     * @param jointPassowrd
     * @return
     */
    public static boolean setJointUser(BankAccount account, String jointEmail, String jointPassowrd) {
        if(UserDaoImpl.signIn(jointEmail, jointPassowrd) == null){
            return false;
        }
        String sql = "update bank_account set shared_user_email = ? where card_number = ?";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);) {
            ps.setString(1, jointEmail);
            ps.setString(2, account.getCardNumber());
            int success = ps.executeUpdate();
            if (success > 0) {
                System.out.println("add/change authorized user (joint account) successfully");
                return true;
            } else {
                return false;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    /***
     * get all bank account belongs to a given user
     * @param user_email
     * @return
     */
    public static ArrayList<String>  getAllExistingAccount(String user_email) {
        String sql = "select * from bank_account where user_email = ? or shared_user_email = ?";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);) {
            ps.setString(1, user_email);
            ps.setString(2, user_email);
            ResultSet rs = ps.executeQuery();
            int num = 1;
            System.out.println("Below are your existing bank accounts");
            ArrayList<String> listOfBank = new ArrayList<String>();
            while (rs.next()) {
                listOfBank.add(rs.getString("card_number"));
                String checkingOrSaving = " Type: Checking";
                if(rs.getBoolean("is_saving")){
                    checkingOrSaving = " Type: Saving";
                }
                System.out.println(Integer.toString(num) + ". " + rs.getString("card_number") + checkingOrSaving);
                num += 1;
            }
            if(num == 1){
                System.out.println("You have no account under user " + user_email + ". Please open an account first");
                return null;
            }
            System.out.println("Please enter your choice");
            return listOfBank;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    /***
     * add a transactionto table record(transaction history)
     * @param amount
     * @param cardNumber
     * @return
     */
    public static boolean addRecord(Double amount, String cardNumber){
        String sql = "insert into record (amount, record_time, card_number) values (?,?,?)";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);) {
            ps.setDouble(1, amount);
            Calendar calendar = Calendar.getInstance();
            Date now = calendar.getTime();
            java.sql.Timestamp currentTime = new java.sql.Timestamp(now.getTime());
            ps.setTimestamp(2, currentTime);
            ps.setString(3, cardNumber);
            int success = ps.executeUpdate();

            if(success>0){
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    /***
     * get the bank account balance for given card number
     * @param cardNumber
     * @return
     */
    public static double getTargetBalance(String cardNumber){
        String sql = "select balance from bank_account where card_number = ?";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);) {
            ps.setString(1,cardNumber);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                return rs.getDouble("balance");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return -1;
    }

    /***
     * transfer money from one account to another account
     * @param account
     * @param amount
     * @param targetCardNumber
     * @return
     */
    public static boolean transfer(BankAccount account, Double amount, String targetCardNumber){
        String sql = "update bank_account set balance = ? where card_number = ?";
        String url = "jdbc:postgresql://training-db.cvaaoud1no8t.us-east-2.rds.amazonaws" + ".com:5432/postgres";
        final String PASSWORD = System.getenv("PASSWORD");
        // If use ConnectionUtil.getConnection(), second executeUpdate() statement will raise error: This connection has been closed.
        try (Connection connection = DriverManager.getConnection(url, "postgres", PASSWORD);
             PreparedStatement ps1 = connection.prepareStatement(sql);
             PreparedStatement ps2 = connection.prepareStatement(sql);){
            connection.setAutoCommit(false);
            Double newBalance = account.getBalance() - amount;
            if(newBalance < 0){
                System.out.println("transfer failed: overdraft");
                return false;
            }

            ps1.setDouble(1, newBalance);
            ps1.setString(2, account.getCardNumber());
            ps1.executeUpdate();
            account.setBalance(newBalance);

            Double newBalanceTarget = getTargetBalance(targetCardNumber) + amount;
            ps2.setDouble(1, newBalanceTarget);
            ps2.setString(2, targetCardNumber);
            ps2.executeUpdate();
            connection.commit();
            addRecord(amount * (-1), account.getCardNumber());
            addRecord(amount, targetCardNumber);
            System.out.println("transfer successfully");
        } catch (PSQLException e){
            System.out.println("The card number you enter does not exist");
        }catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return false;
    }

    /***
     * deposit money to this account
     * @param account
     * @param amount
     * @return
     */
    public static boolean deposit(BankAccount account, Double amount) {
        String sql = "update bank_account set balance = ? where card_number = ?";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);) {
            Double newBalance = account.getBalance() + amount;
            ps.setDouble(1, newBalance);
            ps.setString(2, account.getCardNumber());
            int success = ps.executeUpdate();
            account.setBalance(newBalance);
            if (success > 0 && addRecord(amount, account.getCardNumber())) {
                System.out.println("deposit successfully");
                return true;
            } else {
                System.out.println("failed: deposit");
                return false;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    /***
     * withdraw money from this account
     * @param account
     * @param amount
     * @return
     */
    public static boolean withdraw(BankAccount account, Double amount) {
        String sql = "update bank_account set balance = ? where card_number = ?";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);) {
            Double newBalance = account.getBalance() - amount;
            if(newBalance < 0){
                System.out.println("failed to withdraw: overdraft");
                return false;
            }
            ps.setDouble(1, newBalance);
            ps.setString(2, account.getCardNumber());
            int success = ps.executeUpdate();
            account.setBalance(newBalance);
            if (success > 0 && addRecord(amount * (-1), account.getCardNumber())) {
                System.out.println("deposit successfully");
                return true;
            } else {
                System.out.println("failed: deposit");
                return false;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    /***
     * view the transaction history for an account
     * @param cardNumber
     */
    public static void displayTransactionHistory(String cardNumber){
        String sql = "select * from record where card_number = ?";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);) {
            ps.setString(1, cardNumber);
            ResultSet rs = ps.executeQuery();

            System.out.println("Transaction history:");
            while (rs.next()) {
                String str = "";
                str = str + rs.getTimestamp("record_time") +": ";
                Double amount = rs.getDouble("amount");
                if(amount > 0){
                    str = str + "+ $" + String.format("%.2f", amount);
                }else{
                    str = str + "- $" + String.format("%.2f", amount*(-1));
                }
                System.out.println(str);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
}
