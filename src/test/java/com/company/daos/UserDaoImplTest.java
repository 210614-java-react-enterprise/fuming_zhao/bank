package com.company.daos;

import com.company.models.BankAccount;
import com.company.util.ConnectionUtil;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

public class UserDaoImplTest {

    private BankAccount ba = new BankAccount("1111111111111111",true, 1000, "e5");

    @BeforeAll
    public static void runSetup() throws SQLException, FileNotFoundException {
        try(Connection connection = ConnectionUtil.getConnection()){
            RunScript.execute(connection, new FileReader("setup.sql"));
        }
    }
    @Test
    public void createBankAccountTestNotNull(){
        // check if we create new bank account successfully
        assertNotNull(BankAccountDaoImpl.createBankAccount("33333333333333333333", false, 0, "e5"));
    }

    @Test
    public void getBankAccountTestNotNull(){
        // check if what we get back from the db is not null
        assertNotNull(BankAccountDaoImpl.getBankAccount("1111111111111111"));
    }

    @Test
    public void addRecordTestNotNull(){
        // check if what we get back from the db is null
        assertTrue(BankAccountDaoImpl.addRecord(100.0, "1111111111111111"));
    }

    @Test
    public void depositTestTrue(){
        // check if we deposit successfully
        assertTrue(BankAccountDaoImpl.deposit(ba, 100.0 ));
    }


    @Test
    public void withdrawTestOverdraft(){
        // check overdrafting
        assertFalse(BankAccountDaoImpl.withdraw(ba, 5000.00 ));
    }

    @Test
    public void getTargetBalanceTest(){
        // check if the balance we get from database is correct
        assertEquals(BankAccountDaoImpl.getTargetBalance("2222222222222222"), 1000);
    }
    

    @AfterAll
    public static void runTeardown() throws SQLException, FileNotFoundException {
        try(Connection connection = ConnectionUtil.getConnection()){
            RunScript.execute(connection, new FileReader("teardown.sql"));
        }
    }
}
